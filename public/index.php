<!DOCTYPE html>
<html>
	<head>
		<title>Page Title</title>
		<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	    <link href="assets/build/style/style.css" rel="stylesheet">
	</head>
	<body>

	<div class="container-fluid">


		<div class="row boxes row--no-gutter">
			<div class="col--no-gutter col-md-6 col-sm-4 col-xs-12">
				<div class="box-row red"></div>
			</div>

			<div class="col--no-gutter col-md-3 col-sm-4 col-xs-12">
				<div class="box-row orange">Lovely Box</div>
			</div>

			<div class="col--no-gutter col-md-3 col-sm-4 col-xs-12">
				<div class="box-row green">
					

				</div>
			</div>
		</div>

		<div class="l-header">
			<?php include_once('view/components/c-hero.php'); ?>
		</div>

		<div class="row boxes row--no-gutter">
			<div class="col--no-gutter col-md-2 col-sm-4 col-xs-12">
				<div class="box-row blue">Lovely Box</div>
			</div>

			<div class="col--no-gutter col-md-6 col-sm-4 col-xs-12">
				<div class="box-row purple">Lovely Box</div>
			</div>

			<div class="col--no-gutter col-md-4 col-sm-4 col-xs-12">
				<div class="box-row pink">Lovely Box</div>
			</div>
		</div>

		<div class="row boxes">
			<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
				<div class="box-row red">Lovely Box</div>
			</div>

			<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
				<div class="box-row orange">Lovely Box</div>
			</div>

			<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
				<div class="box-row green">Lovely Box</div>
			</div>

			<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
				<div class="box-row blue">Lovely Box</div>
			</div>

			<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
				<div class="box-row orange">Lovely Box</div>
			</div>

			<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
				<div class="box-row purple">Lovely Box</div>
			</div>
		</div>

	</div>

	<script src="assets/build/script/main.min.js"></script>
	</body>
</html>
